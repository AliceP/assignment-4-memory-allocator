#include "mem.h"
#include "mem_internals.h"

const int32_t TEST_MEMORY_SIZE = 9000;

#define ASSERT(condition, test_name) if (!(condition)) { \
         printf("Test \"%s\" failed!\n", test_name);     \
                     heap_term();\
         return;\
}

void test_alloc_success() {
    char* test_name = "alloc success";

    heap_init(1);
    void* memory = _malloc(250);
    struct block_header* header = block_get_header(memory);

    ASSERT(header->capacity.bytes == 250, test_name);
    _free(memory);
    heap_term();
    printf("Test \"%s\" passed!\n", test_name);
}

void test_free_one_block() {
    char* test_name = "free one block";

    heap_init(1);
    void* memory = _malloc(100);
    void* memory1 = _malloc(200);
    void* memory2 = _malloc(300);
    _free(memory);

    ASSERT(block_get_header(memory)->is_free, test_name)
    ASSERT(!block_get_header(memory1)->is_free, test_name)
    ASSERT(!block_get_header(memory2)->is_free, test_name)
    _free(memory2);
    _free(memory1);
    heap_term();

    printf("Test \"%s\" passed!\n", test_name);
}

void test_free_some_blocks() {
    char* test_name = "free some blocks";

    heap_init(1);
    void* memory = _malloc(100);
    block_capacity capacity = block_get_header(memory)->next->capacity;
    void* memory1 = _malloc(20);
    void* memory2 = _malloc(300);

    _free(memory2);
    _free(memory1);

    ASSERT(block_get_header(memory1)->capacity.bytes == capacity.bytes, test_name)
    heap_term();

    printf("Test \"%s\" passed!\n", test_name);
}

void test_alloc_new_region() {
    char* test_name = "alloc new region";
    heap_init(1);
    void* memory = _malloc(TEST_MEMORY_SIZE);
    struct block_header* header = block_get_header(memory);
    ASSERT(header->capacity.bytes == TEST_MEMORY_SIZE && header->next->capacity.bytes > REGION_MIN_SIZE, test_name)

    printf("Test \"%s\" passed!\n", test_name);
}

void test_alloc_new_region_somewhere() {
    char* test_name = "alloc new region somewhere";
    void* odd_alloc = mmap(HEAP_START + REGION_MIN_SIZE, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE
                                                                                      | MAP_ANONYMOUS, -1, 0 );
    heap_init(1);
    uint8_t* mem = _malloc(TEST_MEMORY_SIZE);
    struct block_header* header = block_get_header(mem);

    ASSERT(header != odd_alloc && header->capacity.bytes == TEST_MEMORY_SIZE, test_name);

    munmap(odd_alloc, 100);
    heap_term();
    printf("Test \"%s\" passed!\n", test_name);
}

int main() {
    test_alloc_success();
    test_free_one_block();
    test_free_some_blocks();
    test_alloc_new_region();
    test_alloc_new_region_somewhere();

    return 0;
}
